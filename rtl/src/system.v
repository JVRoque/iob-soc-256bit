`timescale 1 ns / 1 ps

`include "system.vh"

module system (
               input                     clk,
               input                     reset,
               output                    trap,

               //address write
               output [0:0]              m_axi_awid, 
               output [`MEM_ADDR_W-1:0]  m_axi_awaddr,
               output [7:0]              m_axi_awlen,
               output [2:0]              m_axi_awsize,
               output [1:0]              m_axi_awburst,
               output [0:0]              m_axi_awlock,
               output [3:0]              m_axi_awcache,
               output [2:0]              m_axi_awprot,
               output [3:0]              m_axi_awqos,
               output                    m_axi_awvalid,
               input                     m_axi_awready,

               //write
               output [`MEM_DATA_W-1:0]  m_axi_wdata,
               output [`MEM_WSTRB_W-1:0] m_axi_wstrb,
               output                    m_axi_wlast,
               output                    m_axi_wvalid, 
               input                     m_axi_wready,

               //write response
               input [0:0]               m_axi_bid,
               input [1:0]               m_axi_bresp,
               input                     m_axi_bvalid,
               output                    m_axi_bready,

               //address read
               output [0:0]              m_axi_arid,
               output [`MEM_ADDR_W-1:0]  m_axi_araddr, 
               output [7:0]              m_axi_arlen,
               output [2:0]              m_axi_arsize,
               output [1:0]              m_axi_arburst,
               output [0:0]              m_axi_arlock,
               output [3:0]              m_axi_arcache,
               output [2:0]              m_axi_arprot,
               output [3:0]              m_axi_arqos,
               output                    m_axi_arvalid, 
               input                     m_axi_arready,

               //read
               input [0:0]               m_axi_rid,
               input [`MEM_DATA_W-1:0]   m_axi_rdata,
               input [1:0]               m_axi_rresp,
               input                     m_axi_rlast, 
               input                     m_axi_rvalid, 
               output                    m_axi_rready,
               //UART
               output                    uart_txd,
               input                     uart_rxd,
               output                    uart_rts,
               input                     uart_cts
               );

   //
   // RESET
   //
   wire                             soft_reset;   
   wire                             reset_int = reset | soft_reset;
   reg                              boot ;   
   
   //
   //  CPU
   //
   wire [`ADDR_W-1:0]               m_addr;
   wire [`DATA_W-1:0]               m_wdata;
   wire [3:0]                       m_wstrb;
   wire [`DATA_W-1:0]               m_rdata;
   wire                             m_valid;
   wire                             m_ready;
   wire                             m_instr;
   wire [`MEM_ADDR_W-1:$clog2(`MEM_WSTRB_W)] s_mem_addr;
   wire [`MEM_DATA_W-1:0]           s_mem_wdata;
   wire [`MEM_WSTRB_W-1:0]          s_mem_wstrb;
   wire [`MEM_DATA_W-1:0]           s_mem_rdata;
   wire                             s_mem_valid;
   wire                             s_mem_ready;
   picorv32 #(
              .ENABLE_PCPI(1), //enables the following 2 parameters
	      .ENABLE_FAST_MUL(1),
	      .ENABLE_DIV(1)
	      )
   picorv32_core (
		  .clk           (clk),
		  .resetn        (~reset_int),
		  .trap          (trap),
		  //memory interface
		  .mem_valid     (m_valid),
		  .mem_instr     (m_instr),
		  .mem_ready     (m_ready),
		  .mem_addr      (m_addr),
		  .mem_wdata     (m_wdata),
		  .mem_wstrb     (m_wstrb),
		  .mem_rdata     (m_rdata),
                  // Look-Ahead
                  .mem_la_read   (),
                  .mem_la_write  (),
                  .mem_la_addr   (),
                  .mem_la_wdata  (),
                  .mem_la_wstrb  (),
                  // Pico Co-Processor PCPI
                  .pcpi_valid    (),
                  .pcpi_insn     (),
                  .pcpi_rs1      (),
                  .pcpi_rs2      (),
                  .pcpi_wr       (1'b0),
                  .pcpi_rd       (32'd0),
                  .pcpi_wait     (1'b0),
                  .pcpi_ready    (1'b0),
                  // IRQ
                  .irq           (32'd0),
                  .eoi           (),
                  .trace_valid   (),
                  .trace_data    ()
		  );

   //
   //ADDRESS TRANSLATOR
   //
   //choose main memory according to reset condition
   reg [`N_SLAVES_W-1 : 0]                m_addr_int;

   always @*
     if (!boot && !m_addr[`ADDR_W-1 -: `N_SLAVES_W])
       m_addr_int = `N_SLAVES_W'd`CACHE_BASE;
     else
       m_addr_int = m_addr[`ADDR_W-1 -:`N_SLAVES_W];

   
   //
   // INTERCONNECT
   //
   
   wire [`DATA_W-1:0]                     s_rdata[`N_SLAVES-1:0];
   wire [`N_SLAVES*`DATA_W-1:0]            s_rdata_concat;
   wire [`N_SLAVES-1:0]                   s_valid;
   wire [`N_SLAVES-1:0]                   s_ready;
   
   //concatenate slave read data signals to input in interconnect
   genvar                                 i;
   generate 
         for(i=0; i<`N_SLAVES; i=i+1)
           begin : rdata_concat
	      assign s_rdata_concat[((i+1)*`DATA_W)-1 -: `DATA_W] = s_rdata[i];
           end
   endgenerate

   iob_generic_interconnect 
     #(.N_SLAVES(`N_SLAVES),
       .N_SLAVES_W(`N_SLAVES_W)
       )
   generic_interconnect
     (
      // master interface
      .m_addr  (m_addr_int),
      .m_rdata (m_rdata),
      .m_valid (m_valid),
      .m_ready (m_ready),
      
      // slaves interface
      .s_rdata (s_rdata_concat),
      .s_valid (s_valid),
      .s_ready (s_ready)
      );

   
   //
   // BOOT RAM
   //

   ram  #(
	  .ADDR_W(`BOOT_ADDR_W-2),
          .NAME("boot")
	  )
   boot_mem (
	     .clk           (clk ),
             .rst           (reset_int),
	     .wdata         (m_wdata),
	     .addr          (m_addr[`BOOT_ADDR_W-1:2]),
	     .wstrb         (m_wstrb),
	     .rdata         (s_rdata[`BOOT_BASE]),
             .valid         (s_valid[`BOOT_BASE]),
             .ready         (s_ready[`BOOT_BASE])
	     );


   //
   // UART
   //
   iob_uart uart(
		 //cpu interface
		 .clk       (clk),
		 .rst       (reset_int),
                 
		 //cpu i/f
		 .sel       (s_valid[`UART_BASE]),
		 .ready     (s_ready[`UART_BASE]),
		 .address   (m_addr[4:2]),
		 .read      (m_wstrb == 0),
		 .write     (m_wstrb != 0),
                 
		 .data_in   (m_wdata),
		 .data_out  (s_rdata[`UART_BASE]),
                 
		 //serial i/f
		 .txd       (uart_txd),
		 .rxd       (uart_rxd),
                 .rts       (uart_rts),
                 .cts       (uart_cts)
		 );
   
   //
   // RESET CONTROLLER
   //
   reg        rst_ctrl_rdy;
   reg [7:0] soft_reset_cnt;
   
   always @(posedge clk, posedge reset)
     if(reset)  begin
        boot <= 1'b1;
        soft_reset_cnt <= 8'h0;
        rst_ctrl_rdy <= 1'b0;
     end else if( s_valid[`SOFT_RESET_BASE] && m_wstrb ) begin
        soft_reset_cnt <= 8'hFF;
        boot <=  m_wdata[0];
        rst_ctrl_rdy <= 1'b1;
     end else if (soft_reset_cnt) begin
        soft_reset_cnt <= soft_reset_cnt - 1'b1;
        rst_ctrl_rdy <= 1'b0;
     end

   assign soft_reset = (soft_reset_cnt != 16'h0); 
   assign s_ready[`SOFT_RESET_BASE] = rst_ctrl_rdy;
   assign s_rdata[`SOFT_RESET_BASE] = 0;
   

   //
   // MAIN MEMORY
   //
`ifdef L2
      L2_ID_1sp #(
                  .ADDR_W(`CACHE_ADDR_W),
                  .DATA_W(`DATA_W),
                  .MEM_NATIVE(`MEM_NATIVE),
                  .LA_INTERF(0),
                  .MEM_ADDR_W(`MEM_ADDR_W),
                  .MEM_DATA_W(`MEM_DATA_W),
                  .L1_LINE_OFF_W(`LINE_OFF_W),
                  .L2_LINE_OFF_W(`LINE_OFF_W),
                  .L1_WORD_OFF_W(`WORD_OFF_W),
                  .L2_WORD_OFF_W(`WORD_OFF_W),
                  .L2_N_WAYS(`N_WAYS),
                  .REP_POLICY(`REP_POLICY)
               )
   cache (
	  .clk                (clk),
	  .reset              (reset_int),
      
          //data interface 
	  .wdata   (m_wdata),
	  .addr    ({1'b0, m_addr[`CACHE_ADDR_W-2:2]}), // so when connected to MIG (CACHE_ADDR_W 30), it doesn't receive the N_SLAVES_W MSB used in memory mapping), CACHE_CTRL doesn't have that problem
          .wstrb   (m_wstrb),
          .rdata   (s_rdata[`CACHE_BASE]),
          .valid   (s_valid[`CACHE_BASE] | s_valid[`CACHE_CTRL_BASE]),
          .ready   (s_ready[`CACHE_BASE]),
          .instr   (m_instr),
          .select  (s_valid[`CACHE_CTRL_BASE]),
          //
	  // AXI INTERFACE
          //
          //address write
          .axi_awid(m_axi_awid), 
          .axi_awaddr(m_axi_awaddr), 
          .axi_awlen(m_axi_awlen), 
          .axi_awsize(m_axi_awsize), 
          .axi_awburst(m_axi_awburst), 
          .axi_awlock(m_axi_awlock), 
          .axi_awcache(m_axi_awcache), 
          .axi_awprot(m_axi_awprot),
          .axi_awqos(m_axi_awqos), 
          .axi_awvalid(m_axi_awvalid), 
          .axi_awready(m_axi_awready), 
          //write
          .axi_wdata(m_axi_wdata), 
          .axi_wstrb(m_axi_wstrb), 
          .axi_wlast(m_axi_wlast), 
          .axi_wvalid(m_axi_wvalid), 
          .axi_wready(m_axi_wready), 
          //write response
          .axi_bid(m_axi_bid), 
          .axi_bresp(m_axi_bresp), 
          .axi_bvalid(m_axi_bvalid), 
          .axi_bready(m_axi_bready), 
          //address read
          .axi_arid(m_axi_arid), 
          .axi_araddr(m_axi_araddr), 
          .axi_arlen(m_axi_arlen), 
          .axi_arsize(m_axi_arsize), 
          .axi_arburst(m_axi_arburst), 
          .axi_arlock(m_axi_arlock), 
          .axi_arcache(m_axi_arcache), 
          .axi_arprot(m_axi_arprot), 
          .axi_arqos(m_axi_arqos), 
          .axi_arvalid(m_axi_arvalid), 
          .axi_arready(m_axi_arready), 
          //read 
          .axi_rid(m_axi_rid), 
          .axi_rdata(m_axi_rdata), 
          .axi_rresp(m_axi_rresp), 
          .axi_rlast(m_axi_rlast), 
          .axi_rvalid(m_axi_rvalid),  
          .axi_rready(m_axi_rready),
          //////////////////
          //Native Interface
          //////////////////
          .mem_addr(s_mem_addr),
          .mem_wdata(s_mem_wdata),
          .mem_wstrb(s_mem_wstrb),
          .mem_rdata(s_mem_rdata),
          .mem_valid(s_mem_valid),
          .mem_ready(s_mem_ready)
	  );

   assign s_rdata[`CACHE_CTRL_BASE] = s_rdata[`CACHE_BASE];
   assign s_ready[`CACHE_CTRL_BASE] = s_ready[`CACHE_BASE];
   

   
`else
   
   iob_cache #(
               .ADDR_W(`CACHE_ADDR_W),
               .DATA_W(`DATA_W),
               .MEM_NATIVE(`MEM_NATIVE),
               .LA_INTERF(0),
               .MEM_ADDR_W(`MEM_ADDR_W),
               .MEM_DATA_W(`MEM_DATA_W),
               .LINE_OFF_W(`LINE_OFF_W),
               .WORD_OFF_W(`WORD_OFF_W),
               .N_WAYS(`N_WAYS),
               .REP_POLICY(`REP_POLICY)
               )
   cache (
	  .clk                (clk),
	  .reset              (reset_int),
      
          //data interface 
	  .wdata   (m_wdata),
	  .addr    ({1'b0, m_addr[`CACHE_ADDR_W-2:2]}), // so when connected to MIG (CACHE_ADDR_W 30), it doesn't receive the N_SLAVES_W MSB used in memory mapping), CACHE_CTRL doesn't have that problem
          .wstrb   (m_wstrb),
          .rdata   (s_rdata[`CACHE_BASE]),
          .valid   (s_valid[`CACHE_BASE] | s_valid[`CACHE_CTRL_BASE]),
          .ready   (s_ready[`CACHE_BASE]),
          .instr   (m_instr),
          .select  (s_valid[`CACHE_CTRL_BASE]),
          //
	  // AXI INTERFACE
          //
          //address write
          .axi_awid(m_axi_awid), 
          .axi_awaddr(m_axi_awaddr), 
          .axi_awlen(m_axi_awlen), 
          .axi_awsize(m_axi_awsize), 
          .axi_awburst(m_axi_awburst), 
          .axi_awlock(m_axi_awlock), 
          .axi_awcache(m_axi_awcache), 
          .axi_awprot(m_axi_awprot),
          .axi_awqos(m_axi_awqos), 
          .axi_awvalid(m_axi_awvalid), 
          .axi_awready(m_axi_awready), 
          //write
          .axi_wdata(m_axi_wdata), 
          .axi_wstrb(m_axi_wstrb), 
          .axi_wlast(m_axi_wlast), 
          .axi_wvalid(m_axi_wvalid), 
          .axi_wready(m_axi_wready), 
          //write response
          .axi_bid(m_axi_bid), 
          .axi_bresp(m_axi_bresp), 
          .axi_bvalid(m_axi_bvalid), 
          .axi_bready(m_axi_bready), 
          //address read
          .axi_arid(m_axi_arid), 
          .axi_araddr(m_axi_araddr), 
          .axi_arlen(m_axi_arlen), 
          .axi_arsize(m_axi_arsize), 
          .axi_arburst(m_axi_arburst), 
          .axi_arlock(m_axi_arlock), 
          .axi_arcache(m_axi_arcache), 
          .axi_arprot(m_axi_arprot), 
          .axi_arqos(m_axi_arqos), 
          .axi_arvalid(m_axi_arvalid), 
          .axi_arready(m_axi_arready), 
          //read 
          .axi_rid(m_axi_rid), 
          .axi_rdata(m_axi_rdata), 
          .axi_rresp(m_axi_rresp), 
          .axi_rlast(m_axi_rlast), 
          .axi_rvalid(m_axi_rvalid),  
          .axi_rready(m_axi_rready),
          //////////////////
          //Native Interface
          //////////////////
          .mem_addr(s_mem_addr),
          .mem_wdata(s_mem_wdata),
          .mem_wstrb(s_mem_wstrb),
          .mem_rdata(s_mem_rdata),
          .mem_valid(s_mem_valid),
          .mem_ready(s_mem_ready)
	  );

   assign s_rdata[`CACHE_CTRL_BASE] = s_rdata[`CACHE_BASE];
   assign s_ready[`CACHE_CTRL_BASE] = s_ready[`CACHE_BASE];
`endif // !`ifdef L2

   

   iob_sp_mem_be #(
		   .COL_WIDTH(8),
		   .NUM_COL(`MEM_DATA_W/8),
                   .ADDR_WIDTH(`MEMRAM_ADDR_W-$clog2(`MEM_WSTRB_W))
                   )
   iob_gen_memory
     (
      .din(s_mem_wdata),
      .addr(s_mem_addr[`MEMRAM_ADDR_W-1 : $clog2(`MEM_WSTRB_W)]),
      .we(s_mem_wstrb), 
      .en(s_mem_valid),
      .clk(clk), 
      .dout(s_mem_rdata)
      );

   reg                                       aux_mem_ready;
   assign s_mem_ready = aux_mem_ready;
   
   always @(posedge clk) 
     begin
        aux_mem_ready <= s_mem_valid; 
     end  

  
endmodule
