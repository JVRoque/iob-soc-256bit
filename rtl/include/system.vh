//
// HARDWARE DEFINITIONS
//

//uart 
`define UART_CLK_FREQ 100000000

//Optional memories (passed as command line macro)
`define USE_DDR
`define USE_INTER
`define MEM_NATIVE 0
`define L2

`define CACHE_ADDR_W 30
`define LINE_OFF_W 2
`define WORD_OFF_W 4
`define N_WAYS 1
`define REP_POLICY 0

`define MEM_DATA_W 256
`define MEM_WSTRB_W 32 //MEM_DATA_W/8
`define MEM_ADDR_W 30
//(AXI and Native) RAM sizes (needs to be equal or smaller than MEM_ADDR_W)
`define MEMRAM_ADDR_W 16 //default 16

// slaves
// minimum 3 slaves: boot, uart and reset
// optional ram, and ddr (cache + cache_ctr)
`define N_SLAVES 5

//bits reserved to identify slave
`define N_SLAVES_W 3

//peripheral address prefixes
`define BOOT_BASE 0
`define UART_BASE 1
`define SOFT_RESET_BASE 2
`define RAM_BASE 3
`define CACHE_BASE 3
`define CACHE_CTRL_BASE 4

//address width
`define ADDR_W 32

//data width
`define DATA_W 32

//boot memory address space (log2 of byte size)
//if no RAM or DDR set to same size as RAM
`define BOOT_ADDR_W 12

//main memory address space (log2 of byte size)
`define RAM_ADDR_W 16

