`timescale 1ns / 1ps

`include "system.vh"
`include "iob-uart.vh"

module system_tb;

   //clock
   reg clk = 1;
   always #5 clk = ~clk;

   //reset 
   reg reset = 1;

   // program memory 
   reg [31:0] progmem[`PROG_SIZE/4-1:0];

   //uart signals
   reg [7:0]  rxread_reg = 8'b0;
   reg [2:0]  uart_addr;
   reg        uart_sel;
   reg        uart_wr;
   reg        uart_rd;
   reg [31:0] uart_di;
   reg [31:0] uart_do;

   //cpu to receive getchar
   reg [7:0]  cpu_char = 0;

   integer    i;

   
   //
   // TEST PROCEDURE
   //
   initial begin

`ifdef VCD
      $dumpfile("system.vcd");
      $dumpvars();
`endif

      //init cpu bus signals
      uart_sel = 0;
      uart_wr = 0;
      uart_rd = 0;
      
      // deassert rst
      repeat (100) @(posedge clk);
      reset <= 0;

      //sync up with reset 
      repeat (100) @(posedge clk) #1;

      //
      // CONFIGURE UART
      //
      cpu_inituart();

      //
      // LOAD FIRMWARE
      //

 `include "boot_tb.v"

      //
      // DO THE TEST
      //


`include "firmware_tb.v"


   end // test procedure
   

   //
   // INSTANTIATE COMPONENTS
   //
   
   wire       tester_txd, tester_rxd;       
   wire       tester_rts, tester_cts;       
   wire       trap;              
   //Write address
   wire [0:0] sys_awid;
   wire [`MEM_ADDR_W-1:0] sys_awaddr;
   wire [7:0]  sys_awlen;
   wire [2:0]  sys_awsize;
   wire [1:0]  sys_awburst;
   wire        sys_awlock;
   wire [3:0]  sys_awcache;
   wire [2:0]  sys_awprot;
   wire [3:0]  sys_awqos;
   wire        sys_awvalid;
   wire        sys_awready;
   //Write data
   wire [`MEM_DATA_W-1:0] sys_wdata;
   wire [`MEM_WSTRB_W-1:0] sys_wstrb;
   wire        sys_wlast;
   wire        sys_wvalid;
   wire        sys_wready;
   //Write response
   wire [0:0]  sys_bid;
   wire [1:0]  sys_bresp;
   wire        sys_bvalid;
   wire        sys_bready;
   //Read address
   wire [0:0]  sys_arid;
   wire [`MEM_ADDR_W-1:0] sys_araddr;
   wire [7:0]  sys_arlen;
   wire [2:0]  sys_arsize;
   wire [1:0]  sys_arburst;
   wire        sys_arlock;
   wire [3:0]  sys_arcache;
   wire [2:0]  sys_arprot;
   wire [3:0]  sys_arqos;
   wire        sys_arvalid;
   wire        sys_arready;
   //Read data
   wire [0:0]  sys_rid;
   wire [`MEM_DATA_W-1:0] sys_rdata;
   wire [1:0]  sys_rresp;
   wire        sys_rlast;
   wire        sys_rvalid;
   wire        sys_rready;

   
   //
   // UNIT UNDER TEST
   //
   system uut (
	       .clk              (clk),
	       .reset            (reset),

               //.led             (led),
	       .trap             (trap),
               //DDR
               //address write
	       .m_axi_awid    (sys_awid),
	       .m_axi_awaddr  (sys_awaddr),
	       .m_axi_awlen   (sys_awlen),
	       .m_axi_awsize  (sys_awsize),
	       .m_axi_awburst (sys_awburst),
	       .m_axi_awlock  (sys_awlock),
	       .m_axi_awcache (sys_awcache),
	       .m_axi_awprot  (sys_awprot),
	       .m_axi_awqos   (sys_awqos),
	       .m_axi_awvalid (sys_awvalid),
	       .m_axi_awready (sys_awready),

	       //write  
	       .m_axi_wdata   (sys_wdata),
	       .m_axi_wstrb   (sys_wstrb),
	       .m_axi_wlast   (sys_wlast),
	       .m_axi_wvalid  (sys_wvalid),
	       .m_axi_wready  (sys_wready),

	       //write response
	       .m_axi_bid     (sys_bid),
	       .m_axi_bresp   (sys_bresp),
	       .m_axi_bvalid  (sys_bvalid),
	       .m_axi_bready  (sys_bready),
               
	       //address read
	       .m_axi_arid    (sys_arid),
	       .m_axi_araddr  (sys_araddr),
	       .m_axi_arlen   (sys_arlen),
	       .m_axi_arsize  (sys_arsize),
	       .m_axi_arburst (sys_arburst),
	       .m_axi_arlock  (sys_arlock),
	       .m_axi_arcache (sys_arcache),
	       .m_axi_arprot  (sys_arprot),
	       .m_axi_arqos   (sys_arqos),
	       .m_axi_arvalid (sys_arvalid),
	       .m_axi_arready (sys_arready),

	       //read   
	       .m_axi_rid     (sys_rid[0]),
	       .m_axi_rdata   (sys_rdata),
	       .m_axi_rresp   (sys_rresp),
	       .m_axi_rlast   (sys_rlast),
	       .m_axi_rvalid  (sys_rvalid),
	       .m_axi_rready  (sys_rready),	
               //UART
	       .uart_txd         (tester_rxd),
	       .uart_rxd         (tester_txd),
	       .uart_rts         (tester_cts),
	       .uart_cts         (tester_rts)
	       );


   //TESTER UART
   iob_uart test_uart (
		       .clk       (clk),
		       .rst       (reset),
      
		       .sel       (uart_sel),
		       .address   (uart_addr),
		       .write     (uart_wr),
		       .read      (uart_rd),
		       .data_in   (uart_di),
		       .data_out  (uart_do),

		       .txd       (tester_txd),
		       .rxd       (tester_rxd),
		       .rts       (tester_rts),
		       .cts       (tester_cts)
		       );


   

   
   axi_ram 
      #(
      .DATA_WIDTH (`MEM_DATA_W),
      .ADDR_WIDTH (`MEM_ADDR_W),
      .ID_WIDTH (1)
      )
         ddr_model_mem(
                         //address write
                         .clk            (clk),
                         .rst            (reset),
		         .s_axi_awid     (sys_awid),
		         .s_axi_awaddr   (sys_awaddr),
                         .s_axi_awlen    (sys_awlen),
                         .s_axi_awsize   (sys_awsize),
                         .s_axi_awburst  (sys_awburst),
                         .s_axi_awlock   (sys_awlock),
		         .s_axi_awprot   (sys_awprot),
		         .s_axi_awcache  (sys_awcache),
     		         .s_axi_awvalid  (sys_awvalid),
		         .s_axi_awready  (sys_awready),

		         //write  
		         .s_axi_wvalid   (sys_wvalid),
		         .s_axi_wready   (sys_wready),
		         .s_axi_wdata    (sys_wdata),
		         .s_axi_wstrb    (sys_wstrb),
                         .s_axi_wlast    (sys_wlast),

		         //write response
		         .s_axi_bready   (sys_bready),
                         .s_axi_bid      (sys_bid),
                         .s_axi_bresp    (sys_bresp),
		         .s_axi_bvalid   (sys_bvalid),

		         //address read
		         .s_axi_arid     (sys_arid),
		         .s_axi_araddr   (sys_araddr),
		         .s_axi_arlen    (sys_arlen), 
		         .s_axi_arsize   (sys_arsize),    
                         .s_axi_arburst  (sys_arburst),
                         .s_axi_arlock   (sys_arlock),
                         .s_axi_arcache  (sys_arcache),
                         .s_axi_arprot   (sys_arprot),
		         .s_axi_arvalid  (sys_arvalid),
		         .s_axi_arready  (sys_arready),

		         //read   
		         .s_axi_rready   (sys_rready),
		         .s_axi_rid      (sys_rid),
		         .s_axi_rdata    (sys_rdata),
		         .s_axi_rresp    (sys_rresp),
                         .s_axi_rlast    (sys_rlast),
		         .s_axi_rvalid   (sys_rvalid)
                         );   
   
   //
   // CPU TASKS
   //
   
   // 1-cycle write
   task cpu_uartwrite;
      input [3:0]  cpu_address;
      input [31:0] cpu_data;

      # 1 uart_addr = cpu_address;
      uart_sel = 1;
      uart_wr = 1;
      uart_di = cpu_data;
      @ (posedge clk) #1 uart_wr = 0;
      uart_sel = 0;
   endtask //cpu_uartwrite

   // 2-cycle read
   task cpu_uartread;
      input [3:0]   cpu_address;
      output [31:0] read_reg;

      # 1 uart_addr = cpu_address;
      uart_sel = 1;
      uart_rd = 1;
      @ (posedge clk) #1 read_reg = uart_do;
      @ (posedge clk) #1 uart_rd = 0;
      uart_sel = 0;
   endtask //cpu_uartread


   task cpu_loadfirmware;
      input [`DATA_W-1:0] N_WORDS;
      integer             i, j, k;
      
      $readmemh("firmware.hex", progmem, 0, N_WORDS-1);

      k=0;
      
      for(i=0; i<N_WORDS; i++) begin
	 for(j=1; j<=4; j=j+1) begin
	    cpu_putchar(progmem[i][j*8-1 -: 8]);            
	 end

         if(i == (N_WORDS*k/100) ) begin
            $display("%d%% ", k);
            k=k+10;
         end
      end
      $display("%d%%", 100);
   endtask
   

   task cpu_inituart;
      //pulse reset uart 
      cpu_uartwrite(`UART_SOFT_RESET, 1);
      cpu_uartwrite(`UART_SOFT_RESET, 0);
      //config uart div factor
      cpu_uartwrite(`UART_DIV, `UART_CLK_FREQ/`UART_BAUD_RATE);
      //enable uart for receiving
      cpu_uartwrite(`UART_RXEN, 1);
   endtask

   task cpu_getchar;
      output [7:0] rcv_char;

      //wait until something is received
      do
	cpu_uartread(`UART_READ_VALID, rxread_reg);
      while(!rxread_reg);

      //read the data 
      cpu_uartread(`UART_DATA, rxread_reg); 

      rcv_char = rxread_reg[7:0];
   endtask


   task cpu_putchar;
      input [7:0] send_char;
      //wait until tx ready
      do
	cpu_uartread(`UART_WRITE_WAIT, rxread_reg);
      while(rxread_reg);
      //write the data 
      cpu_uartwrite(`UART_DATA, send_char); 

   endtask

   task cpu_getline;
      do begin 
         cpu_getchar(cpu_char);
         $write("%c", cpu_char);
      end while (cpu_char != "\n");
   endtask

   
   // finish simulation
   always @(posedge trap)   	 
     #500 $finish;
   
endmodule
