#include "system.h"
#include "iob-uart.h"
#include "iob-cache.h"

//peripheral addresses 
//memory
#define MAIN_MEM (CACHE_BASE<<(ADDR_W-N_SLAVES_W))

//uart
#define UART (UART_BASE<<(ADDR_W-N_SLAVES_W))
//soft reset
#define SOFT_RESET (SOFT_RESET_BASE<<(ADDR_W-N_SLAVES_W))
//cache controller
#define CACHE_CTRL (CACHE_CTRL_BASE<<(ADDR_W-N_SLAVES_W))

//debugging purposes
volatile int* MAIN_MEM_PROG;
volatile char* LOAD_MEM;
volatile int* SYS_RESET;
int main()
{ 
    
  uart_init(UART, UART_CLK_FREQ/UART_BAUD_RATE);
  uart_puts ("Loading program from UART ...\n");
  uart_printf("load_address=%x, prog_size=%d \n", MAIN_MEM, PROG_SIZE);
  LOAD_MEM = (volatile char*) MAIN_MEM;
  SYS_RESET = (volatile int*) SOFT_RESET;
  
  for (int i=0 ; i < PROG_SIZE; i++) {
    //uart_printf("a %d\n", i);
    //char c = uart_getc();
    // uart_printf("%x ", c);
    //RAM_PUTCHAR(MAIN_MEM+i, c)
    LOAD_MEM[i] = uart_getc();
    //uart_printf("c %d\n", i);
  }
  
  //Printing the sent program
  uart_puts("\nPrinting program from Main Memory:\n");
  MAIN_MEM_PROG = (volatile int*) MAIN_MEM;
  for (int i=0; i < PROG_SIZE/4; i++){
    uart_printf("%x\n", MAIN_MEM_PROG[i]);
  }
  uart_puts("Finished printing the Main Memory program\n");
    
  
  uart_puts("Program loaded\n");
  uart_txwait();
  
  //Soft Reset
  SYS_RESET[0] = 0;

  /*
  //A simple program similar to boot for debugging
  uart_init(UART, UART_CLK_FREQ/UART_BAUD_RATE);

  uart_puts ("Write chars\n");
  MAIN_MEM_PROG = (volatile int*) MAIN_MEM;
  LOAD_MEM = (volatile char*) MAIN_MEM;
  for (int i=0 ; i < 2000; i++) {
  LOAD_MEM[i] = (char)i;
  }
     
  uart_puts ("Print int\n");
  for (int i=0; i < 2000/4; i++){
  uart_printf("%x\n", MAIN_MEM_PROG[i]);
  }
   
  uart_puts ("END\n");
  */ 
}

